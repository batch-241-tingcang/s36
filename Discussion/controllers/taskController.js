//Controllers contains the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access methods to perform CRUD operations
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/task");

// Controller function for GETTING ALL THE TASKS
// Defines the functions to be used in the "taskRoutes.js" file and export thse functions

module.exports.getAllTasks = () => {

	// The "then" method is used to wait for the Mongoose "find" to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then(result => { 
		return result;
	})
}


module.exports.createTask = (requestBody) => {

	//Creates a task object based on the Mongoose Model "task"
	let newTask = new Task({
		// Sets the "name" property with the value received from the client
		name: requestBody.name
	})

	//the first parameter will store the result return by the Mongoose save method
	//the second parameter will store the "error" object
	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			return task
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

module.exports.updateTask = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}

		result.name = reqBody.name;

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
			} else {
				return updatedTask
			}
		})
	})

}


// ----- ACTIVITY ----- //

// Create a controller function for retrieving a specific task.
// Process a GET request at the /tasks/:id route using postman to get a specific task.

module.exports.getSpecificTasks = (taskId) => {

	return Task.findOne({_id:taskId}).then(result => { 
		return result;
	})
}

// Create a controller function for changing the status of a task to complete.

module.exports.completeTask = (taskId) => {

	return Task.updateOne({_id:taskId}, {status: "complete"}).then(result => { 
	
	return Task.findOne({_id:taskId}).then(result => { 
		return result;
	})

	})
}



