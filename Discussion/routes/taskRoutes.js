//Contains all the endpoints for our application 
//We separate the routes such that "index.js" only contains the information on the server

const express = require("express");

// Creates a Router instance that functions as a middleware and routing system
// Allow access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

const taskController = require("../controllers/taskController");

// Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller function that will be used when a route is accessed.
// They invoke the controller function from the controller files
// All the business logic is done in the controller

// Route to GET ALL THE TASK
router.get("/", (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

})

// Route for CREATING A NEW TASK
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for DELETING A TASK
router.delete("/:id", (req, res) => {

	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route for UPDATING A TASK
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Use "module.exports" to export the router object to use in the "index.js"
module.exports = router;




// ------- ACTIVITY ------- /

//Create a route for getting a specific task.
router.get("/:id", (req, res) => {

	taskController.getSpecificTasks(req.params.id).then(resultFromController => res.send(resultFromController));
});

//Create a route for changing the status of a task to complete.
router.put("/:id/complete", (req, res) => {
	taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
})












