// Set up the  dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoutes.js"
const taskRoutes = require("./routes/taskRoutes");


// Server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection
// Connecting to our MongoDB Atlas
mongoose.set('strictQuery', false)
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.o0hvxs8.mongodb.net/s36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Add the task route

app.use("/tasks", taskRoutes);
//http://localhost:4000/tasks


// What port to listen to?
app.listen(port,() => console.log(`Now listening to port ${port}`));










